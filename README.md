**# Accutics Code Challenge**__



****##Prerequisites****__
__
This is an api project designed with Springboot and Java to test api endpoints.

PostgressDB is required and it has to be installed locally. Make sure you override the username and password in application.properties.

**##API**
GET
/user/findbyemail/{email}

Returs the user by email provided as path variable

Example: htttp://localhost:8080/user/findbyemail/test@test.com



**GET**__
/findbynamecontains/{name}

Returns the list of users by name containing the sequence of characters provided in the name path variable

Example: htttp://localhost:8080/user/findbynamecontains/an



**POST**__
/campaign/create/{usedID}

Creates a campaign and assigns it to a user base on the userID provided as path variable

Example: htttp://localhost:8080/campaign/create/2



**PUT**
__
/campaign/asigninputs/{campaignID}

Request Body: List of inputs represented as a JSON object

Assigns a list of inputs to a campaign based on the campaignID provided as path variable

Example: http://localhost:8080/campaign/asigninputs/4
[
    {
        "type":"CHANNEL",
        "value":"value1"    
    },
    {
        "type":"SOURCE",
        "value":"value2"    
    }
]


**GET**__
/campaign/getall

Request Params: pageNo, pageSize, sortBy, ascending

Returns the campaings paginated by page number and page size. Sorts the campaigns by the input provided in the sortBy query parameter and ordonates it as needed using the ascending query parameter.

Example:
http://localhost:8080/campaign/getall?pageNo=0&pageSize=10&sortBy=creationTime&ascending=true


**##Architecture**__
Entities - the Java and DB model
- User
- Campaign
- Input
Repositories - interfaces for interacting with DB
Services - business logic of the application
Controllers - exposes endpoints as rest resources

**##Tests**
__
I tested the /user/findbyemail/{email} endpoint using HTTPClient.
I ran a request to that endpoint and compared the returned user name with the expected user name. 






package com.acustics.codechallenge;

import org.apache.http.HttpEntity;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.hamcrest.Matchers;
import org.json.JSONException;
import org.json.JSONObject;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.net.http.HttpResponse;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class UserControllerTest {

    @Test
    public void givenUserEmail_whenUserInformationIsRequested_thenRetrievedResourceIsCorrect()
            throws ClientProtocolException, IOException {

        // Given
        HttpUriRequest request = new HttpGet( "http://localhost:8080/user/findbyemail/dasda@dasda" );

        // When
        CloseableHttpResponse response = HttpClientBuilder.create().build().execute(request);
        HttpEntity entity = response.getEntity();
        // Then
        String content = EntityUtils.toString(entity);
        try {
            JSONObject jsonObject = new JSONObject(content);
            assertEquals("Ion",jsonObject.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

}

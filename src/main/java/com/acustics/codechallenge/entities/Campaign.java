package com.acustics.codechallenge.entities;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;
import java.util.Set;


@Entity
public class Campaign {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    @Column
    private String creationTime ;

    @ManyToOne
    @JsonBackReference(value = "userToCampaign")
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "campaign")
    @JsonManagedReference(value="campaignToInput")
    private Set<Input> inputList;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Set<Input> getInputList() {
        return inputList;
    }

    public void setInputList(Set<Input> inputList) {
        this.inputList = inputList;
    }

    public String getCreationTime() {
        return creationTime;
    }

    public void setCreationTime(String creationTime) {
        this.creationTime = creationTime;
    }
}

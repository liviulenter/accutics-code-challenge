package com.acustics.codechallenge.entities;

public enum InputType {
    CHANNEL,
    SOURCE,
    CAMPAIGN_NAME,
    TARGET_URL

}

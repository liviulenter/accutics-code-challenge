package com.acustics.codechallenge.services;

import com.acustics.codechallenge.entities.Input;
import com.acustics.codechallenge.repositories.CampaignRepository;
import com.acustics.codechallenge.repositories.InputRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

@Service
public class InputService {

    @Autowired
    InputRepository inputRepository;

    @Autowired
    CampaignRepository campaignRepository;

    public void assignInputsToCampaign(Set<Input> inputs, Integer campaignId){
        for (Input input: inputs){
            input.setCampaign(campaignRepository.findById(campaignId).get());
            inputRepository.save(input);
        }
    }
}

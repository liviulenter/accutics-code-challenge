package com.acustics.codechallenge.services;

import com.acustics.codechallenge.entities.Campaign;
import com.acustics.codechallenge.entities.Input;
import com.acustics.codechallenge.entities.User;
import com.acustics.codechallenge.repositories.CampaignRepository;
import com.acustics.codechallenge.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class CampaignService {
    @Autowired
    CampaignRepository campaignRepository;
    @Autowired
    UserRepository userRepository;

    public Campaign createCampaign(Integer userId) {

        User user = userRepository.getById(userId);
        Campaign campaign = new Campaign();
        campaign.setUser(user);
        campaign.setCreationTime(new Date().toString());
        return campaignRepository.save(campaign);

    }

    public Campaign asingInputs(Set<Input> inputList, Integer campaignId) throws Exception {
        Optional<Campaign> campaignOptional = campaignRepository.findById(campaignId);
        Campaign campaign;
        if (campaignOptional.isPresent()) {
            campaign = campaignOptional.get();
            campaign.setInputList(inputList);
            return campaignRepository.save(campaign);
        } else {
            throw new Exception("Campaign not found");
        }

    }

    public List<Campaign> getAllCampaigns(Integer pageNo, Integer pageSize, String sortBy, Boolean ascending)
    {

        Pageable paging;
        if (ascending==true) {
            paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).ascending());
        }
        else{
            paging = PageRequest.of(pageNo, pageSize, Sort.by(sortBy).descending());
        }

        Page<Campaign> pagedResult = campaignRepository.findAll(paging);

        if(pagedResult.hasContent()) {
            return pagedResult.getContent();
        } else {
            return new ArrayList<Campaign>();
        }
    }




}



package com.acustics.codechallenge.services;

import com.acustics.codechallenge.entities.User;
import com.acustics.codechallenge.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService {
    @Autowired
    UserRepository userRepository;

    public User findUserByEmail(String emailAddress){
        return userRepository.findUserByEmail(emailAddress);

    }

    public List<User> findUsersByNameContains(String name){
        return userRepository.findUsersByNameContains(name);
    }

}

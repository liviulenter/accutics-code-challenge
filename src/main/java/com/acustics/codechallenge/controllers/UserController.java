package com.acustics.codechallenge.controllers;

import com.acustics.codechallenge.entities.User;
import com.acustics.codechallenge.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("user")
public class UserController {
    @Autowired
    UserService userService;

    @GetMapping("/findbyemail/{email}")
    public ResponseEntity<User> findUserByEmail(@PathVariable("email") String email) {
        User user = userService.findUserByEmail(email);
        if (user == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(user);
        }


    }


    @GetMapping("/findbynamecontains/{name}")
    public ResponseEntity<List<User>> findUserByNameContains(@PathVariable("name") String name) {
       List<User> users = userService.findUsersByNameContains(name);
        if (users == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(users);
        }


    }
}

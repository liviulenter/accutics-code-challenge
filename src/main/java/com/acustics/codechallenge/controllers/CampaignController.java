package com.acustics.codechallenge.controllers;

import com.acustics.codechallenge.entities.Campaign;
import com.acustics.codechallenge.entities.Input;
import com.acustics.codechallenge.repositories.InputRepository;
import com.acustics.codechallenge.services.CampaignService;
import com.acustics.codechallenge.services.InputService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Set;

@Controller
@RequestMapping("campaign")
public class CampaignController {

    @Autowired
    CampaignService campaignService;

    @Autowired
    InputService inputService;

    @PostMapping("/create/{userId}")
    public ResponseEntity<Campaign> create(@PathVariable Integer userId) {
        Campaign createdCammpaign = campaignService.createCampaign(userId);
        if (createdCammpaign == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(createdCammpaign);
        }
    }

    @PutMapping("/asigninputs/{campaignId}")
    public ResponseEntity<Campaign> update(@PathVariable Integer campaignId, @RequestBody Set<Input> inputs) {
        try {
            inputService.assignInputsToCampaign(inputs, campaignId);
            Campaign updatedCampaign = campaignService.asingInputs(inputs, campaignId);
            return ResponseEntity.ok(updatedCampaign);
        } catch (Exception e) {
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping("/getall")
    public ResponseEntity<List<Campaign>> getCampaigns(@RequestParam Integer pageNo,@RequestParam Integer pageSize,@RequestParam String sortBy, @RequestParam Boolean ascending){
        List<Campaign> campaignList = campaignService.getAllCampaigns(pageNo,pageSize,sortBy,ascending);
        if (campaignList == null) {
            return ResponseEntity.notFound().build();
        } else {
            return ResponseEntity.ok(campaignList);
        }

    }

}

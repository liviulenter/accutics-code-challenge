package com.acustics.codechallenge.repositories;

import com.acustics.codechallenge.entities.Input;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputRepository extends JpaRepository<Input,Integer> {
}

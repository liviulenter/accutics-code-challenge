package com.acustics.codechallenge.repositories;

import com.acustics.codechallenge.entities.Campaign;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignRepository extends PagingAndSortingRepository<Campaign,Integer> {



}

package com.acustics.codechallenge.repositories;

import com.acustics.codechallenge.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User,Integer> {

    public User findUserByEmail (String email);
    public List<User> findUsersByNameContains (String name);


}
